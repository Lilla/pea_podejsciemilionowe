//
//  TimeCounter.cpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/12/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#include "TimeCounter.hpp"
#include <ctime>


TimeCounter::TimeCounter()
{
   
}

void TimeCounter::start()
{
    tstart = clock();
    measurement = 0;
}


void TimeCounter::stop()
{
    tstop = clock();
    measurement = ((double)(tstop - tstart))/CLOCKS_PER_SEC;
}

double TimeCounter::read()
{
    return measurement;
}

