//
//  CostMatrix.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/10/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef CostMatrix_hpp
#define CostMatrix_hpp

#include <iostream>

class MatrixCost{
    int citiesNumber;
    int ** distances;
    
    bool flagGood;
    
    void setZero();
    void setInfinity();
    
    
public:
    MatrixCost();
    MatrixCost(int n);
    ~MatrixCost();
    MatrixCost(const MatrixCost& obj);
    
    
    
    void init(int);
    void show();
    int getNumberVertices();
    int getCost(int, int);
    int reduceCostColumn();
    int reduceCostRow();
    void reduceMatrix(int, int);
    bool isGood();
    
    MatrixCost * copy() const; //klonowanie obiektu klasy i zwraca wskaznik do sklonowanego obiektu
};

#endif /* CostMatrix_hpp */
