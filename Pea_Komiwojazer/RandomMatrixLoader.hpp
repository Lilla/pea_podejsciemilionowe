//
//  RandomMatrixLoader.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/8/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef RandomMatrixLoader_hpp
#define RandomMatrixLoader_hpp

#include <iostream>
#include <stdlib.h>
#include "matrix.hpp"

class RandomMatrixLoader{
public:    Matrix run(int cityNumber);
    
};

#endif /* RandomMatrixLoader_hpp */
