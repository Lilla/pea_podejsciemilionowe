//
//  UserInterface.cpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/2/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#include "UserInterface.hpp"
void UserInterface::menu(){
    int choice;
    TSPBruteForce tspBF;
    TSPBranchandBound* tspBB=new TSPBranchandBound;
    bool start=false;
    MatrixOfDistances* matrix=new MatrixOfDistances;
    FileLoader* fileLoader=new FileLoader;
    RandomMatrixLoader randomLoader;
    vector<int> path;
    Matrix matrix1;
    TimeCounter clock;
    
    

    
    while(!start){
        cout<<"M E N U"<<endl;
        cout<<"1-read from file"<<endl;
        cout<<"2-generate random matrix"<<endl;
        cout<<"3-Brute Force"<<endl;
        cout<<"4-Branch and Bound"<<endl;
        
        cout<<"0-exit"<<endl;
        
        cout<<"What would you like to do? "<<endl;
        
        cin>>choice;
        
        
        switch(choice){
            case 1:
            {
                string fileName;
                cout<<"Podaj nazwę pliku "<<endl;
                cin>>fileName;
            matrix->loadFromfile(fileName);
                
                
            }
                break;
                
            case 2:
            {
                int citiesNumber;
                cout<<"How many cities?"<<endl;
                cin>>citiesNumber;
                matrix->randomMatrix(citiesNumber);
            }
                
            case 3:
            {
               matrix1=fileLoader->run("10");
                clock.start();
                tspBF.bruteForce(matrix1);
                clock.stop();
                cout<<clock.read()<<endl;
            }
                break;
                
            case 4:
            {
                int metodeNumb;
                cout<<"Choose metode"<<endl;
                cout<<"1-metod 1"<<endl;
                cout<<"2-metod 2"<<endl;
                cin>>metodeNumb;
                
           /**  path= tspBB->findPath(matrix);
            
                for(int i = 0; i<path.size()-1;i++){
                    if(i!=0)
                        cout<<" - ";
                    cout<<path[i];}
                cout<<endl;*/
                clock.start();
             path= tspBB->findPathTwo(matrix);
                clock.stop();
               
                    for(int i=0; i<path.size()-1;i++){
                        if(i!=0){
                            cout<<" - ";}
                        cout<<path[i];
            }
                cout<<endl;
                cout<<clock.read()<<endl;
            }
                
            break;
           
            case 0:
            {
                 start=true;
            }
                 break;
        
        
        
    }
    
}
}

