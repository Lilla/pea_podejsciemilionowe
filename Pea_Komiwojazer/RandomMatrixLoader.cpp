//
//  RandomMatrixLoader.cpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/8/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#include "RandomMatrixLoader.hpp"
Matrix RandomMatrixLoader::run(int citiesNumber){
    
    Matrix matrix;
    srand(time(NULL));
    //zainicjalizowanie macierzy kwadratowej o rozmiarze takim jak ilość miast, gotowej do inicjalizowania randomowymi wartościami
  
    matrix.distances=new int*[citiesNumber];
    for(int i=0; i<citiesNumber; i++){
        
        matrix.distances[i]=new int[citiesNumber];
    }
    
    for(int j=0; j<citiesNumber;j++){
        for(int i=0; i<citiesNumber;i++)
        {
            matrix.distances[j][i]=rand()%50+2;
            if(i==j){
                matrix.distances[j][i]=-1;
            }
        }
     }
    for (int i=0; i<citiesNumber; i++){
        for(int j=0; j<citiesNumber; j++)
        {
            cout<<matrix.distances[i][j]<<" ";
        }
        cout<<endl;
    }
    return matrix;
}
