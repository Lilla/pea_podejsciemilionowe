//
//  matrix.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/2/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef matrix_hpp
#define matrix_hpp

#include <iostream>
using namespace std;
//W przypadku problemu komiwojażera, moja macierz zawsze jest kwadratowa, jej szerokosc i długość wyznacza ilość miast
typedef struct Matrix{

    int citiesNumber;
    int ** distances;
    int reducedCost=0;
   long int shortestDistances=INT_MAX;
    
  
};

#endif /* matrix_hpp */
