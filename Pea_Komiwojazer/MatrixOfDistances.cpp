//
//  MatrixOfDistances.cpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/10/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#include "MatrixOfDistances.hpp"
using namespace std;
MatrixOfDistances::MatrixOfDistances(){
    this->distances=nullptr;
    this->citiesNumber = 0;
    this->flagGood = false;
}
MatrixOfDistances::MatrixOfDistances(int cityNumb){
    this->init(citiesNumber);
    this->flagGood = false;
}
MatrixOfDistances::~MatrixOfDistances(){
    for (int i=0; i<this->citiesNumber; i++){
        delete [] this->distances[i];
    }
    
    delete [] this->distances;
}
MatrixOfDistances::MatrixOfDistances(const MatrixOfDistances& matrix){
    int cityNumber = matrix.citiesNumber;
    this->init(cityNumber);
    for(int i=0;i<cityNumber;i++){
        ::copy(&matrix.distances[i][0] ,&matrix.distances[i][cityNumber], &this->distances[i][0]);
    }
}

void MatrixOfDistances::init(int citiesNumber){
    this->citiesNumber=citiesNumber;
    this->distances= new int*[citiesNumber];
   for(int i=0; i<citiesNumber; i++){
        this->distances[i]=new int[citiesNumber];
    }
}
void MatrixOfDistances:: display(){
    for (int i=0; i<this->citiesNumber; i++){
        for(int j=0; j<this->citiesNumber; j++)
        {
            cout<<this->distances[i][j]<<" ";
        }
        cout<<endl;
        
    }
}
int MatrixOfDistances::getCityNumber(){
    return this->citiesNumber;
}
int  MatrixOfDistances:: getDistance(int startCity, int destination){
    return this->distances[startCity][destination];
}
int MatrixOfDistances:: reduceCostRow(){
    int minValue;
    int cost=0;
    
    for(int i=0; i<this->citiesNumber; i++){
         minValue=INT_MAX;
        for(int k=0; k<this->citiesNumber; k++){
            if(minValue>this->distances[i][k] && this->distances[i][k]>=0){
                minValue=this->distances[i][k];
            }
        }
        for(int k=0; k<this->citiesNumber; k++){
            if( minValue>0 && minValue!=INT_MAX){
                this->distances[i][k]=this->distances[i][k]-minValue;}
        }
        if(minValue!=INT_MAX && minValue>0)
           cost=+minValue;
        
    }
    return cost;
}
int MatrixOfDistances::reduceCostColumn(){
    int minValue;
    int cost=0;
    for(int i=0; i<this->citiesNumber; i++){
         minValue=INT_MAX;
        for(int k=0; k<this->citiesNumber; k++){
            if(minValue>this->distances[k][i] && this->distances[k][i]>=0){
                minValue=this->distances[k][i];
            }
            
        }
        if(minValue!=INT_MAX){
            cost=+minValue;}
        
        
        for(int k=0; k<this->citiesNumber; k++){
            if( this->distances[k][i]>0 && this->distances[k][i]!=0){
                this->distances[k][i]=this->distances[k][i]-minValue;}
        }
        
    }
    return cost;
    
    }

void MatrixOfDistances::reduceMatrix(int startCity, int destination){
    
    for(int i=0; i<this->citiesNumber; i++){
        this->distances[startCity][i]=-1;
        this->distances[i][destination]=-1;
    }
    
   this->distances[destination][startCity]=-1;
}
bool MatrixOfDistances:: isGood(){
    
    return this->flagGood;
}

MatrixOfDistances* MatrixOfDistances::copy()const{
    
    return new MatrixOfDistances(*this);
    
}

void MatrixOfDistances::loadFromfile(string name){
    int j=0;
    
    fstream file;
    name = name + ".txt";
    
    
    
    file.open("/Users/lilalomnicka/Desktop/"+name);
    
    if(file.is_open())
    {
        cout<<"Plik" << name <<" jest otwarty"<<endl;
    }
    else
    {
        cout<<"Błąd. Pliku nie udalo sie otworzyc pliku wejsciowego"<<endl;
    }
    
    if(file.good())
    {
        
        file >> this->citiesNumber;
        init(citiesNumber);
        while(!file.eof())
        {
            for(int i=0; i<this->citiesNumber;i++)
            {
                file >> this->distances[j][i];
            }
            j++;
        }
        file.close();
     }
    this->display();
}

void MatrixOfDistances::randomMatrix(int citiesNumb){
    srand(time(NULL));
    //zainicjalizowanie macierzy kwadratowej o rozmiarze takim jak ilość miast, gotowej do inicjalizowania randomowymi wartościami
    this->~MatrixOfDistances();
    this->init(citiesNumb);
    for(int j=0; j<this->citiesNumber;j++){
        for(int i=0; i<this->citiesNumber;i++)
        {
            this->distances[j][i]=rand()%50+2;
            if(i==j){
                this->distances[j][i]=-1;
            }
        }
    }
    for (int i=0; i<this->citiesNumber; i++){
        for(int j=0; j<this->citiesNumber; j++)
        {
            cout<<this->distances[i][j]<<" ";
        }
        cout<<endl;
    }
}
