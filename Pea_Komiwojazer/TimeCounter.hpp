//
//  TimeCounter.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/12/18.
//  Copyright © 2018 Lila. All rights reserved.
//
#include <ctime>
#ifndef TimeCounter_hpp
#define TimeCounter_hpp
class TimeCounter
{
public:
    TimeCounter();
    void start();
    void stop();
    double read();
    double measurement;
private:
    clock_t tstart;
    clock_t tstop;
};


#endif /* TimeCounter_hpp */
