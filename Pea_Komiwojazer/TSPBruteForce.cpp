//
//  TSPBruteForce.cpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/2/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#include "TSPBruteForce.hpp"
#include "matrix.hpp"
#include "TSPBruteForce.hpp"
#include "matrix.hpp"
void TSPBruteForce::bruteForce(Matrix matrix){
    
    int *cities=new int[matrix.citiesNumber];
    this->bestRoute=new int[matrix.citiesNumber+1];
    for(int i=0; i<matrix.citiesNumber+1; i++){
        bestRoute[i]=0;
    }
    for(int i=0; i<matrix.citiesNumber; i++)
    {
        cities[i]=i;
    }
    
    permutation(cities, 1, matrix.citiesNumber, matrix);
    displayPermutation(bestRoute, matrix);
}

void TSPBruteForce::swap(int *tab, int i, int j) {
    int tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
}

int TSPBruteForce::totalDistance(int *cities, Matrix matrix) {
    
    int totalDistance=0;
    
    for(int i=0; i<matrix.citiesNumber-1; i++){
        int city1=cities[i];
        int city2=cities[i+1];
        
        totalDistance=totalDistance+ matrix.distances[city1][city2];
        
        
    }
    totalDistance+=matrix.distances[matrix.citiesNumber-1][0];
    
    
    return totalDistance;
}

void TSPBruteForce::permutation(int *tab, int left, int right, Matrix matrix) {
    int actualDist;
    if(left == right) {
        
        
        actualDist=totalDistance(tab, matrix);
        if(actualDist<this->bestRoute[0] || bestRoute[0]==0){
            bestRoute[0]=actualDist;
            for(int i=1; i<matrix.citiesNumber+1; i++){
                bestRoute[i]=tab[i-1];
            }
        }
      
        
        
        return;
        
        
    }
    
    for(int i = left; i < right; i++) {
        swap(tab, left, i);
        permutation(tab, left + 1, right, matrix);
        swap(tab, left, i);
    }
    
}

void TSPBruteForce::displayPermutation(int tab[], Matrix matrix){
    
    cout<<"Cost: "<<tab[0]<<endl;
    cout<<"Path: ";
    for(int i=1; i<matrix.citiesNumber+1; i++){
        std::cout<<tab[i]<<" ";
    }
    cout<<"0";
    cout<<endl;
}
