//
//  TSPBruteForce.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/2/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef TSPBruteForce_hpp
#define TSPBruteForce_hpp

#include <iostream>
#include "matrix.hpp"
using namespace std;
class TSPBruteForce{
    
public:
void bruteForce(Matrix matrix1);
    
private:
int* bestRoute;
int shortesDistance; 
void permutation(int tab[], int, int, Matrix);
void displayPermutation(int tab[], Matrix matrix);
void swap(int tab[], int, int);
int totalDistance(int tab[], Matrix matrix);
};

#endif /* TSPBruteForce_hpp */
