//
//  TSPBranchandBound.cpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/4/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#include "TSPBranchandBound.hpp"
TSPBranchandBound::TSPBranchandBound(){
    this->orginalMatrix=nullptr;
}
TSPBranchandBound::~TSPBranchandBound(){
    delete this->orginalMatrix;
}

bool TSPBranchandBound::checkRows(Node node, int city){
    for(int i=0; i<node.path.size()-1; i++){
        if (city == node.path[i]){
            return true;}
    }
    return false;
}

bool TSPBranchandBound::checkColumns(Node node, int city){
    for(int i=1; i<node.path.size(); i++){
        if (city == node.path[i]){
            return true;}
    }
    return false;
}

int TSPBranchandBound::getValue(Node node){
    int value = 0;
    for (int i=0; i < node.path.size()-1; i++){
        value += this->orginalMatrix->getDistance(node.path[i], node.path[i+1]);
    }
    return value;
}

void TSPBranchandBound::setBound(Node &node){
    int min,tmp;
    node.bound = getValue(node);
    
    for (int i=0; i<this->citiesNumber; i++){
        // jesli bylismy juz w jakims wierzcholku to juz go nie odwiedzamy
        if (this->checkRows(node, i)){
            continue;
        }
        min = INT_MAX;
        for (int j=0; j<this->citiesNumber; j++){
            if (this->checkColumns(node, j)){
                continue;
            }
            
            if (i == j){
                continue;
            }
            
            tmp = orginalMatrix->getDistance(i, j);
            if (tmp < min && tmp >= 0){
                min = tmp;
            }
        }
        node.bound += min;
    }}
void TSPBranchandBound::setBoundTwo(Node &node){
    int cost=0;
    this->copiedMatrix = this->orginalMatrix->copy();
    
    cost += this->copiedMatrix->reduceCostRow();
    cost += this->copiedMatrix->reduceCostColumn();
    
    for(int i=0; i <node.path.size()-1;i++){
        cost += this->copiedMatrix->getDistance(node.path[i],node.path[i+1]);
        this->copiedMatrix->reduceMatrix(node.path[i],node.path[i+1]);
        cost += this->copiedMatrix->reduceCostRow();
        cost +=this->copiedMatrix->reduceCostColumn();
       
    }
    node.bound=cost;
}

vector<int>TSPBranchandBound:: findPath(MatrixOfDistances* matrix){
    this->citiesNumber = matrix->getCityNumber();
    this->orginalMatrix = matrix->copy();
    int bestValue = INT_MAX;
    vector<int> tmpPath;
    vector<int> resultPath;
    
    tmpPath.push_back(0);
    this->checkedNode.level = 1;
    this->checkedNode.path = tmpPath;
    this->setBound(this->checkedNode);
    
    
    
    queue.push(this->checkedNode);
    
    while(!queue.empty())
    {
        this->checkedNode = this->queue.top();
        this->queue.pop();
        
        
        if (this->checkedNode.bound < bestValue){
            for (int i=1; i< this->citiesNumber; i++){
                if (!this->checkColumns(this->checkedNode, i)){
                    this->tmpNode.path = this->checkedNode.path;
                    this->tmpNode.path.push_back(i);
                    this->tmpNode.level = this->checkedNode.level+1;
                    this->setBound(this->tmpNode);
                    
                    if (this->tmpNode.level == this->citiesNumber){
                        this->tmpNode.path.push_back(0);
                        this->setBound(this->tmpNode);
                        if (this->tmpNode.bound < bestValue){
                            bestValue = this->tmpNode.bound;
                            resultPath = this->tmpNode.path;
                        }
                    }
                    
                    if (this->tmpNode.bound < bestValue){
                        queue.push(this->tmpNode);
                    }
                }
            }
            
            
        }
    } //end while
    
    resultPath.push_back(bestValue);
    
    return resultPath;
}
vector<int> TSPBranchandBound::findPathTwo(MatrixOfDistances* matrix){
    this->citiesNumber = matrix->getCityNumber();
    this->orginalMatrix = matrix->copy();
    int bestValue = INT_MAX;
    vector<int> tmpPath;
    vector<int> resultPath;
    
    tmpPath.push_back(0);
    this->checkedNode.level = 1;
    this->checkedNode.path = tmpPath;
    this->setBoundTwo(this->checkedNode);
    
    queue.push(this->checkedNode);
    
    while(!queue.empty())
    {
        this->checkedNode = this->queue.top();
        this->queue.pop();
        
        
        if (this->checkedNode.bound < bestValue){
            for (int i=1; i< this->citiesNumber; i++){
                if (!this->checkColumns(this->checkedNode, i)){
                    this->tmpNode.path = this->checkedNode.path;
                    this->tmpNode.path.push_back(i);
                    this->tmpNode.level = this->checkedNode.level +1;
                    this->setBoundTwo(this->tmpNode);
                    
                    if (this->tmpNode.level == this->citiesNumber){
                        this->tmpNode.path.push_back(0);
                        this->setBound(this->tmpNode);
                        if (this->tmpNode.bound < bestValue){
                            bestValue = this->tmpNode.bound;
                            resultPath = this->tmpNode.path;
                        }
                    }
                    
                    if (this->tmpNode.bound < bestValue && this->tmpNode.bound>0){
                        queue.push(this->tmpNode);
                    }
                }
            }
        }
    } //end while
    
    resultPath.push_back(bestValue);

    return resultPath;
    
    
}
