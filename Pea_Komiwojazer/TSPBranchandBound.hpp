//
//  TSPBranchandBound.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/4/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef TSPBranchandBound_hpp
#define TSPBranchandBound_hpp

#include <iostream>
#include "matrix.hpp"
#include <queue>
#include <vector>
#include "Node.hpp"
#include "compareNode.hpp"
#include "MatrixOfDistances.hpp"

using namespace std;

class TSPBranchandBound{
    int citiesNumber;
    Node checkedNode, tmpNode;
    MatrixOfDistances *orginalMatrix, *copiedMatrix;
    priority_queue<Node, vector<Node>, compareNode> queue;
public:
  
    TSPBranchandBound();
    ~TSPBranchandBound();
    
    bool checkRows(Node, int );
    bool checkColumns(Node, int );
    int getValue(Node);
    void setBound(Node&);
    void setBoundTwo(Node&);
    
    vector<int> findPath(MatrixOfDistances*);
    vector<int> findPathTwo(MatrixOfDistances*);
    
};
#endif /* TSPBranchandBound_hpp */
