//
//  FileLoader.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/2/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef FileLoader_hpp
#define FileLoader_hpp

#include <iostream>
#include "matrix.hpp"
#include "MatrixOfDistances.hpp"
using namespace std;

class FileLoader{
    
 
    
public:
  Matrix  run(string name);
    
    
    
};

#endif /* FileLoader_hpp */
