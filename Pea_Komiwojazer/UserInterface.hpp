//
//  UserInterface.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/2/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef UserInterface_hpp
#define UserInterface_hpp

#include <iostream>
#include "matrix.hpp"
#include "TSPBruteForce.hpp"
#include "FileLoader.hpp"
#include "TSPBranchandBound.hpp"
#include "RandomMatrixLoader.hpp"
#include "MatrixOfDistances.hpp"
#include "TimeCounter.hpp"
using namespace std;
class UserInterface{
public:
    void menu();
};

#endif /* UserInterface_hpp */
