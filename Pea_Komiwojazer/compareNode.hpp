//
//  compareNode.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/10/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef compareNode_hpp
#define compareNode_hpp

#include "Node.hpp"
struct compareNode{
    bool operator()(const Node &a, const Node &b)const{
        return a.bound>b.bound;
    }
};

#endif /* compareNode_hpp */
