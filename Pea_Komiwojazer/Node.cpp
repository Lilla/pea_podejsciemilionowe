//
//  Node.cpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/10/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#include "Node.hpp"


Node::Node(){
    this->bound = 0;
    this->value = 0;
    this->level = 0;
}

Node::Node(int b, int v, int l){
    this->bound=b;
    this->value=v;
    this->level=l;
}

Node::~Node(){
    
}
