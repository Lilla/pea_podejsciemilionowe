//
//  MatrixOfDistances.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/10/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef MatrixOfDistances_hpp
#define MatrixOfDistances_hpp


#include <iostream>
#include <fstream>
#include "FileLoader.hpp"
using namespace std;

class MatrixOfDistances{
int citiesNumber;
int ** distances;
bool flagGood;
    
public:
    MatrixOfDistances();
    MatrixOfDistances(int);
    ~MatrixOfDistances();
    MatrixOfDistances(const MatrixOfDistances&);
    
    
    void init(int);
    void display();
    int getCityNumber();
    int getDistance(int, int);
    int reduceCostColumn();
    int reduceCostRow();
    void reduceMatrix(int, int);
    bool isGood();
    void loadFromfile(string);
    void randomMatrix(int citiesNumb);
    
    //metoda która kopiuje macierz i zwraca wskaźnik do skopiowanej macierzy
    MatrixOfDistances* copy() const;
};

#endif /* MatrixOfDistances_hpp */
