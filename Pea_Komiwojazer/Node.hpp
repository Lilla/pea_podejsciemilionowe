//
//  Node.hpp
//  Pea_Komiwojazer
//
//  Created by Lila Łomnicka on 11/10/18.
//  Copyright © 2018 Lila. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <vector>

using namespace std;



class Node{
public:
    int level, bound, value;
    
    vector<int> path;
    
    Node();
    Node(int,int,int);
    ~Node();
};



#endif /* Node_hpp */
